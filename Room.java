
/*
 * This is room class support the main class.
 * This class describes the default identifier for each room and also print out information
 *  It gives detailed information about the each room.
 */
public class Room {

	String floors;
	String walls;
	int windows;
	
	 // Create a room with its content.
	public Room(String floors, String walls, int windows){
		this.floors =floors;
		this.walls = walls;
		this.windows = windows;
	    }
	
	 //Setting the room variable to default. 
	public Room() {
		walls ="";
		floors= "";
		windows= 0;
	}
	
	// Inquiry for the type of floors for the Room.
	public void setFloors(String floors)
	{
		this.floors = floors;
	}

	 // Inquiry for how many windows there is for a room.
	public void setWindows(int windows)

	{
		this.windows = windows;
	}

	 //Inquiry for the type of color for the Room.
	public void setWalls(String walls){

		this.walls = walls;

	}
	//Returns to the room information

	public String getWalls()

	{
		return this.walls;
	}
	public String getFloors(){
		return this.floors;
	}
	public int getWindows(){

		return this.windows;
	}
	
		//Printing out all the information about each room; this.wall, this.floor and this.windows.
	public String toString(){
		return "Color of walls: " + this.walls + "\n" +
				"Type of floors: " + this.floors + "\n" +
				"Number of windows: " + this.windows+ "\n";
	}
}
		
